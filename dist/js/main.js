"use strict";

/* --------------------------------------------
  Consts
---------------------------------------------*/
var twtEase = 'cubic-bezier(.9,0,.1,1)';
var gmapStyle = [{
  "featureType": "administrative",
  "elementType": "labels.text.fill",
  "stylers": [{
    "color": "#444444"
  }]
}, {
  "featureType": "landscape",
  "elementType": "all",
  "stylers": [{
    "color": "#f2f2f2"
  }]
}, {
  "featureType": "landscape.natural",
  "elementType": "all",
  "stylers": [{
    "visibility": "on"
  }, {
    "color": "#e6e6e6"
  }]
}, {
  "featureType": "poi",
  "elementType": "all",
  "stylers": [{
    "visibility": "off"
  }]
}, {
  "featureType": "road",
  "elementType": "all",
  "stylers": [{
    "saturation": -100
  }, {
    "lightness": 45
  }]
}, {
  "featureType": "road",
  "elementType": "geometry.fill",
  "stylers": [{
    "visibility": "on"
  }, {
    "hue": "#ff0000"
  }]
}, {
  "featureType": "road.highway",
  "elementType": "all",
  "stylers": [{
    "visibility": "simplified"
  }]
}, {
  "featureType": "road.highway",
  "elementType": "geometry.fill",
  "stylers": [{
    "color": "#fd9836"
  }, {
    "saturation": "85"
  }, {
    "lightness": "31"
  }, {
    "gamma": "1.24"
  }]
}, {
  "featureType": "road.highway",
  "elementType": "labels.text",
  "stylers": [{
    "visibility": "simplified"
  }]
}, {
  "featureType": "road.arterial",
  "elementType": "geometry.fill",
  "stylers": [{
    "hue": "#ff0000"
  }, {
    "saturation": "1"
  }]
}, {
  "featureType": "road.arterial",
  "elementType": "labels.icon",
  "stylers": [{
    "visibility": "off"
  }]
}, {
  "featureType": "transit",
  "elementType": "all",
  "stylers": [{
    "visibility": "off"
  }]
}, {
  "featureType": "transit.station",
  "elementType": "all",
  "stylers": [{
    "visibility": "simplified"
  }, {
    "hue": "#ff0000"
  }, {
    "saturation": "-100"
  }]
}, {
  "featureType": "transit.station.airport",
  "elementType": "all",
  "stylers": [{
    "visibility": "on"
  }]
}, {
  "featureType": "transit.station.bus",
  "elementType": "all",
  "stylers": [{
    "visibility": "simplified"
  }]
}, {
  "featureType": "transit.station.rail",
  "elementType": "all",
  "stylers": [{
    "visibility": "simplified"
  }, {
    "hue": "#ff7e00"
  }, {
    "saturation": "-100"
  }, {
    "lightness": "19"
  }]
}, {
  "featureType": "water",
  "elementType": "all",
  "stylers": [{
    "color": "#d7d7d7"
  }, {
    "visibility": "on"
  }]
}];

/* --------------------------------------------
  FX
---------------------------------------------*/
function fx() {

  var staggerDelay = 80;
  var staggerSettings = {
    distance: '100%',
    viewFactor: 0.5,
    reset: false
  };

  window.sr = ScrollReveal({
    distance: '0',
    duration: 1500,
    reset: true,
    scale: 1,
    delay: 0,
    easing: 'cubic-bezier(.5,0,.1,1)',
    viewFactor: 0.2
  });

  sr.reveal('.reveal');

  sr.reveal('.reveal-up', {
    distance: '40px',
    viewFactor: 0.1,
    reset: false
  });

  sr.reveal('.reveal-right', {
    origin: 'right'
  }, staggerDelay);

  // Setup effetto di comparsa in sequenza
  // Assegna un diverso ID a ogni gruppo 
  // per evitare che partano tutti insieme
  $('[class*="container"]').has('.reveal-up-stagger').each(function (i, e) {
    var groupId = 'reveal-up-stagger--group-' + i;
    $('.reveal-up-stagger', e).addClass(groupId);
    sr.reveal("." + groupId, staggerSettings, staggerDelay);
  });
}

/* --------------------------------------------
  Main nav
---------------------------------------------*/
function navBG() {

  // Aggiunge classe appropriata al .main-nav 
  // per modificare il gradiente di sfondo della navigazione
  // a seconda del contentuto della prima fascia
  $('.hero').length > 0 ? $('.main-nav').addClass('has-hero') : null;

  // Sfondo pagina prodotto
  $('.detail__photo').length > 0 ? $('.main-nav').addClass('is-detail') : null;
}

/* --------------------------------------------
  Google map
-------------------------------------------- */
function rivenditore() {

  var rome = new google.maps.LatLng(41.893, 12.482);

  var map = new google.maps.Map(document.getElementById('rivenditore__gmap'), {
    zoom: 4,
    center: rome,
    styles: gmapStyle
  });
  var marker = new google.maps.Marker({
    position: rome,
    animation: google.maps.Animation.DROP,
    map: map,
    icon: 'img/ui/gmap-marker.png',
    label: {
      text: '22',
      color: 'white',
      fontSize: '11px',
      fontFamily: "Blair Md"
    }
  });
}

/* --------------------------------------------
  Rivenditori
-------------------------------------------- */
function rivenditori() {

  var rome = new google.maps.LatLng(41.893, 12.482);

  var map = new google.maps.Map(document.getElementById('rivenditori__gmap'), {
    zoom: 4,
    center: rome,
    styles: gmapStyle
  });
  var marker = new google.maps.Marker({
    position: rome,
    animation: google.maps.Animation.DROP,
    map: map,
    icon: 'img/ui/gmap-marker.png',
    label: {
      text: '22',
      color: 'white',
      fontSize: '11px',
      fontFamily: "Blair Md"
    }
  });

  var debounced = _.debounce(function () {

    $('.rivenditori__map').each(function (i, e) {
      var overflowWidth = ($(window).innerWidth() - $('.container-fluid').width()) / 2;

      // Settaggio dimensione mappa
      $('.rivenditori__gmap', e).width(function () {
        return "calc(100% + " + overflowWidth + "px)";
      }());
    });
  }, 200);

  $(window).resize(debounced).resize();
}

/* --------------------------------------------
  Modals
-------------------------------------------- */
function modals() {
  $('.twt-fade').on('hide.bs.modal', function (e) {
    var _this = this;

    setTimeout(function () {
      _this.setAttribute("style", "display: none");
    }, 600);
  });
}

/* --------------------------------------------
  Mega Dropdown
---------------------------------------------*/
function megadropdown() {

  var mdd = $('.mdd'),
      mddNav = $('.mdd__nav'),
      desktopBreakpoint = 992;

  var isDesktop = void 0;

  // Setup link primo livello
  $('.mdd__item').filter(function (i, e) {
    return $(e).find('ul').length;
  }).addClass('has-megamenu');

  // Crea codice per il megamenu
  $('.has-megamenu > ul').wrap("\n    <div class=\"mdd__megamenu\">\n      <div class=\"mdd__megamenu__inner\">\n        <div class=\"mdd__megamenu__container mdd__megamenu__container--first\">\n        </div>\n      </div>\n    </div>\n  ");

  // Salva variabile
  var mddFirst = $('.mdd__megamenu__container--first');

  // Crea container laterale
  mddFirst.after('<div class="mdd__megamenu__container mdd__megamenu__container--second"></div>');

  // Setup sottomenu di primo livello 
  $('> ul > li', mddFirst).filter(function (i, e) {
    return $(e).find('ul').length;
  }).addClass('has-submenu');

  // BG nav
  $('.mdd__item.has-megamenu').hover(function () {
    mdd.addClass('open');
  }, function () {
    $('.mdd__megamenu, .has-submenu > a').add(mdd).removeClass('open');
  });

  // Apertura sottomenu
  $('.has-submenu > a', mddFirst).click(function (ev) {
    ev.preventDefault();

    var megamenu = $(this).parents('.mdd__megamenu'),
        sublinks = $(this).next();

    // Rimuove classe "open" dagli altri
    $('.has-submenu > a').not(this).removeClass('open');

    // Toggle <a> class
    $(this).toggleClass('open');

    // Set megamenu class
    megamenu.not('.open').toggleClass('open');

    // Set submenu content on right panel for desktop
    if (isDesktop) {
      megamenu.find('.mdd__megamenu__container--second').html(sublinks[0].outerHTML);
    }
    // Slide toggle for mobile
    else {
        sublinks.slideToggle();
      }
  });

  // Mobile menu toggler
  function closeMobileNav() {
    $('.mdd .hamburger').removeClass('is-active');
    mddNav.add('.main-nav').removeClass('open');
  }

  $('.mdd__mobile-toggle').click(function (ev) {
    $('.mdd .hamburger').toggleClass('is-active');
    mddNav.add('.main-nav').toggleClass('open');
  });

  // Mobile first level toggler
  $('.mdd__item.has-megamenu > a').click(function (ev) {
    ev.preventDefault(); // Avoid click on this demo

    // Mobile: open on click
    if (!isDesktop) {
      $(this).next() // Select .mdd__megamenu
      .slideToggle('fast').find('ul ul').hide() // Hide third level links
      .end().find('.open') // Remove class from groups left open
      .removeClass('open');
    }
  });

  // Impedisce il bubbling dell'evento dai link più interni
  $('.mdd__megamenu a').not('.has-dropdown').click(function (ev) {
    if (!isDesktop) {
      ev.stopImmediatePropagation();
    }
  });

  // Sistemazione larghezza del menù
  var resizeHandler = _.debounce(function () {
    isDesktop = window.matchMedia("(min-width: " + desktopBreakpoint + "px)").matches;

    if (isDesktop) {
      var padding = ($(window).innerWidth() - $('.container-fluid').width()) / 2 + 'px';
      mddNav.css({
        'padding-left': padding,
        'padding-right': padding
      });
      // Set nav visible after size calc
      mddNav.addClass('ready');

      // Close Mobile nav
      closeMobileNav();
    }
  }, 250);

  var scrollHandler = _.debounce(function () {
    // Controlla scroll per compattare il menu
    window.scrollY > 0 ? $('.main-nav .secondary').addClass('secondary--compact') : $('.main-nav .secondary').removeClass('secondary--compact');
  });

  $(window).resize(resizeHandler).resize().scroll(scrollHandler).scroll();
}

/* --------------------------------------------
  Hero
---------------------------------------------*/
function hero() {

  // Stoppa se non è presente l'elemento
  if ($('.hero').length === 0) return;

  Vue.component('hero-slide', {
    data: function data() {
      return {
        isPlaying: false
      };
    },

    props: ['currentSlide'],
    computed: {
      slideIndex: function slideIndex() {
        return this.currentSlide;
      }
    },
    watch: {
      slideIndex: function slideIndex() {
        this.stop();
      }
    },
    methods: {
      playPause: function playPause($event) {
        var video = $('video', this.$el)[0];
        $event.preventDefault();
        video.paused ? video.play() : video.pause();
        this.isPlaying = !this.isPlaying;
      },
      stop: function stop() {
        var video = $('video', this.$el)[0];
        this.isPlaying = false;
        video.load();
      }
    }
  });

  new Vue({
    el: '.hero',
    data: {
      currentSlide: 0
    },
    mounted: function mounted() {
      var vm = this;
      $('.hero .slider').on('init', function (a, b) {

        // Nasconde punti se c'è una sola slide (bug di slick)
        if ($('.hero .slider .slick-slide').length === 1) {
          $(this).css('margin-bottom', 0).find('.hero .slider .slick-dots').remove();
        }
      }).slick({
        dots: true,
        arrows: false,
        fade: true
      }).on('afterChange', function (event, slick) {
        vm.currentSlide = slick.currentSlide;
      });
    }
  });
}

/* --------------------------------------------
  Video overflow
---------------------------------------------*/
function videoOverflow() {

  if ($('.video-overflow').length === 0) return;

  new Vue({
    el: '.video-overflow',
    data: {
      isPlaying: false,
      video: {
        width: 0
      }
    },
    methods: {
      playPause: function playPause($event) {
        var video = $event.target;
        this.isPlaying = !this.isPlaying;
        video.paused ? video.play() : video.pause();
      },
      ended: function ended() {
        this.isPlaying = false;
      }
    },
    mounted: function mounted() {
      var vm = this;
      vm.$nextTick(function () {
        var debounced = _.debounce(function () {
          $('.video-overflow').each(function (i, e) {
            var overflowWidth = ($(window).innerWidth() - $('.container').width()) / 2;

            // Set video size
            $('video', e).width(function () {
              return "calc(100% + " + overflowWidth + "px)";
            }());

            // Set play button position
            $('.video-overflow__play', e).css('left', "calc(50% - (50px + " + overflowWidth / 2 + "px))");
          });
        }, 100);

        $(window).resize(debounced).resize();
      });
    }
  });
}

/* --------------------------------------------
  Fullslider
---------------------------------------------*/
function fullslider() {
  $('.fullslider .slider').slick({
    arrows: false,
    autoplay: true,
    dots: true,
    centerMode: true,
    variableWidth: true,
    autoplaySpeed: 5000,
    speed: 1500,
    responsive: [{
      breakpoint: 576,
      settings: {
        centerMode: false,
        variableWidth: false
      }
    }]
  });
}

/* --------------------------------------------
  Scroll to element
---------------------------------------------*/
function scrollTo() {
  $('[data-scrollto]').click(function (ev) {
    ev.preventDefault();
    var target = this.dataset.scrollto;
    $('html, body').animate({
      scrollTop: $(target).offset().top
    }, 500);
  });
}

/* --------------------------------------------
  Lista corta di prodotti
  Slider su mobile, tre colonne su desktop
---------------------------------------------*/
function productsShortList() {
  var $element = $('.products__short-list');

  // Esci dalla funzione se l'elemento non è presente nella pagina
  if ($element.length === 0) return;

  $element.slick({
    dots: true,
    arrows: true,
    mobileFirst: true,
    slidesToShow: 1,
    infinite: false,
    responsive: [{
      breakpoint: 576,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 992,
      settings: 'unslick'
    }]
  });

  var debounced = _.debounce(function () {
    // Fa ripartire gli slick quando si restringe la finestra
    if (window.matchMedia("(max-width: 991px)").matches) {
      $element.toArray().forEach(function (item) {
        return item.slick.refresh();
      });
    }
  }, 800);

  $(window).resize(debounced);
}

/* --------------------------------------------
  DOC READY
---------------------------------------------*/
$(function () {

  navBG();
  megadropdown();
  fullslider();
  modals();
  scrollTo();
  productsShortList();

  hero();
  videoOverflow();

  if ($('.video-overflow').length) {}

  if ($('[data-page="rivenditori"]').length) {
    rivenditori();
  }

  if ($('[data-page="rivenditore"]').length) {
    rivenditore();
  }

  fx();

  setTimeout(function () {
    return $('html,body').scrollTop(0);
  }, 200);
});