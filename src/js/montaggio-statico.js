/* --------------------------------------------------------
  Funzioni di supporto per il montaggio statico; 
  rimuovere nella versione finale
---------------------------------------------------------*/


function previewPages () {
  var pageName = location.search.slice(1)
  $('[data-page]').not('[data-page="' + pageName + '"]').remove();
  document.body.className = pageName;
}



/* DOC READY */
$( function () {
  previewPages();
});



new Vue({
  el: '.hexagons',
  data: {
    items: [
      {
        id: 'soluzioni',
        label: 'soluzioni su misura'
      },
      {
        id: 'consulenza',
        label: 'consulenza b2b/b2c'
      },
      {
        id: '4life',
        label: 'sistema 4life'
      }
    ]
  }
});



new Vue({
  el: '.chisiamo__eccellenza',
  data: {
    items: [
      'garniga',
      'rekord',
      'zuani'
    ]
  }
});

new Vue({
  el: '[data-page="generico"]'
});


new Vue({
  el: '.rivenditori__list'
});

new Vue({
  el: '[data-page="listing"]'
});

new Vue({
  el: '[data-page="products"]'
});


new Vue({
  el: '[data-page="detail"]'
});


new Vue({
  el: '.filiali'
});

